const rootEl = document.querySelector(":root");
const htmlEl = document.documentElement;
const headerEl = document.querySelector(".header");
const headerLogoEl = headerEl.querySelector(".logo");
const navEl = document.querySelector(".header__nav");
const navToggleEl = document.querySelector(".nav-toggle");
const mainEl = document.querySelector(".main");

const rootStyles = getComputedStyle(rootEl);
const largeScreenReferenceSize =
  +rootStyles.getPropertyValue("--big-screen-reference-size").slice(0, -2) * 16;
const smallScreenReferenceSize =
  +rootStyles.getPropertyValue("--small-screen-reference-size").slice(0, -2) *
  16;
const smallScreenThresholdProp = rootStyles.getPropertyValue(
  "--small-screen-threshold",
);
const smallScreenThreshold = +smallScreenThresholdProp.slice(0, -2) * 16;
const smallScreenMQ = window.matchMedia(
  `only screen and (max-width: ${smallScreenThresholdProp})`,
);
const headerTransitionDuration = +rootStyles
  .getPropertyValue("--header-transition-duration")
  .slice(0, -2);
const emailInputs = document.querySelectorAll(".cta-field__input");

// Makes link in nav toggle disabled when JS is enabled
document.getElementById("menu-link").removeAttribute("href");

/* Accommodate label behavior on focus out from input */
function onInputFocusout(event) {
  const parentEl = event.target.parentElement;
  const labelEl = parentEl.querySelector(".cta-field__label");
  if (event.target.value.length) {
    labelEl.classList.add("input-entered");
  } else {
    labelEl.classList.remove("input-entered");
  }
}

emailInputs.forEach((el) => {
  onInputFocusout({ target: el });
  el.addEventListener("focusout", onInputFocusout);
});

/* Accommodate font on window resize */
function getFontChangeRate(currentSize, referenceSize) {
  if (referenceSize == largeScreenReferenceSize) {
    return currentSize <= referenceSize ? 0.6 : 1;
  }

  return currentSize <= referenceSize ? 0.75 : 0.5;
}

function onResize() {
  const currentSize = window.innerWidth;
  const referenceSize =
    currentSize <= smallScreenThreshold
      ? smallScreenReferenceSize
      : largeScreenReferenceSize;
  const fontChangeRate = getFontChangeRate(currentSize, referenceSize);
  const baseFontMultiplier = Math.pow(
    currentSize / referenceSize,
    fontChangeRate,
  );
  const newBaseFontRatio = baseFontMultiplier * 62.5;
  htmlEl.style.fontSize = `${newBaseFontRatio.toFixed(4)}%`;
}

window.addEventListener("resize", onResize);
onResize();

/* Nav toggle functionality */
function onNavToggle() {
  headerEl.classList.toggle("header--nav-toggled");
  mainEl.classList.toggle("main--nav-toggled");
  const isNavOpen = navToggleEl.classList.toggle("nav-toggle--active");
  if (isNavOpen) {
    navEl.style.visibility = "unset";
  } else {
    setTimeout(() => {
      navEl.style.visibility = "hidden";
    }, headerTransitionDuration);
  }
}

navToggleEl.addEventListener("click", onNavToggle);

/* Restore everything to normal if VW changes from small to large */
function onSmallScreenMQChange() {
  if (!smallScreenMQ.matches) {
    headerEl.classList.remove("header--nav-toggled");
    mainEl.classList.remove("main--nav-toggled");
    navToggleEl.classList.remove("nav-toggle--active");
    navEl.style.visibility = "unset";
    headerLogoEl.style.visibility = "unset";
  }
}

smallScreenMQ.addEventListener("change", onSmallScreenMQChange);
