<h1 align="center">Edit Homepage</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://islambeg-frontend-projects.gitlab.io/vanilla/edie-homepage/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/islambeg-frontend-projects/vanilla/edie-homepage">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/xobQBuf8zWWmiYMIAZe0">
      Challenge
    </a>
  </h3>
</div>

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Features](#features)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)

## Overview

Simple landing page built with HTML, modern CSS and a little bit of vanilla JS.

### Built With

- HTML
- CSS
- [Sass](https://sass-lang.com/)
- JS (Vanilla Flavor)

## Features

JS is used to make:

1. font responsive;
2. input label well-behaved;
3. nav toggle functional (small screens only).

## Acknowledgements

- [A modern CSS Reset](https://piccalil.li/blog/a-modern-css-reset/)
- [Favicon by Remix Design](https://remixicon.com/)
- [Icons by Google](https://google.github.io/material-design-icons/)

## Contact

- Email islambeg@proton.me
- Gitlab [@islambeg](https://gitlab.com/islambeg)
